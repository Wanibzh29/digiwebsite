"use strict";

var api = "http://prv.cymru:13999";

var text;

function s(key){
	if(key in text){
		if(lang in text[key])
			return text[key][lang];
		else
			return text[key]['en'];
	}
	return key;
}

window.onload = function(){
	document.getElementsByTagName('nav')[0].getElementsByTagName('button')[0].click();

	fetch("translations.json")
		.then(res => res.json())
		.then(obj => text = obj);

	if(typeof startUpId !== 'undefined')
		searchById(startUpId);
}

function wordSearch() {
	var input = document.getElementById("search").value.toLowerCase();
	var lang = document.getElementById("lang").value;
	var type = document.getElementById("type").value;
	var search = document.getElementById('selectSearch').value;
	
	var url = api + "/api/dictionary?word=" + input +
		(type != "" ? "&type=" + type : "" ) +
		"&lang=" + lang +
		"&search=" + search;

	fetchAndHandleJsonRequest(url, wordListToDiv);
}

function numberSearch(){
	var number = document.getElementById('numberSearch').value;
	fetchAndHandleJsonRequest(
		api + '/api/dictionary/number?number=' + number,
		convertNumberToDiv);
}

function generateWord(){
	var input = document.getElementById("json").value;
	var lang = document.getElementById("lang").value;
	var type = document.getElementById("type").value;
	
	var url = api + "/api/dictionary/generate" + 
		"?json=" + escape(input) +
		"&type=" + type +
		"&lang=" + lang;

	fetchAndHandleJsonRequest(url, wordListToDiv);
}

function transSearch(){
	var word = document.getElementById("transSearch").value.toLowerCase();
	var url = api + "/api/dictionary/translate" + 
		"?word=" + escape(word);

	fetchAndHandleJsonRequest(url, translationsToDiv);
}

function generateList(){
	var min = document.getElementById("min").value;
	var max = document.getElementById("max").value;
	
	var url = api + "/api/dictionary/idlist" + 
		"?min=" + min +
		"&max=" + max;

	fetchAndHandleJsonRequest(url, idlistToDiv);
}


function openTab(list){
	var tabs = document.getElementsByClassName('tab');
	var i;
	for(i = 0; i < tabs.length; ++i)
		tabs[i].style.display = "none";

	for(i = 0; i < list.length; ++i)
		document.getElementById(list[i]).style.display = "block";
}

function keyPress(enter) {
	enter = enter || window.event;
	if (enter.keyCode == 13) {
		if(enter.target.id == "search")
			wordSearch();
		else if(enter.target.id == "json")
			generateWord();
		else if(enter.target.id == "transSearch")
			transSearch();
		else if(enter.target.id == "newWord")
			addWordToList();
		else
			numberSearch();
		return false;
	}
	return true;
}

function searchById(id){
	fetchAndHandleJsonRequest(api + "/api/dictionary?id=" + id, wordListToDiv);
}

function createClickableWord(text, id){
	var button = document.createElement('button');
	button.classList.add('clickableWord');
	button.onclick = function(){searchById(id)};
	button.innerHTML = text;
	return button;
}

function fetchAndHandleJsonRequest(url, callback){
	console.log("Fetching: " + url);
	fetch(url)
		.then(res => res.json())
		.then((out) => {
			handleResponse(out, callback)
		})
		.catch(err => {
			// Incase we fail to contact or convert JSON from API, give the user an error message.
			var results = document.createElement("div")
			results.id = "results";
			var div = document.createElement("div")
			div.classList.add('result');
			div.appendChild(createTextElement('h2', 'No connection to API :('));
			div.appendChild(createTextElement('h3', 'Reason: ' + err.message));
			results.appendChild(div);
			var body = document.getElementsByTagName("body")[0];
			var oldResult = document.getElementById("results");
			body.replaceChild(results, oldResult);
		});
}

function createResultsDiv(){
	var results = document.createElement("div")
	results.id = "results";
	results.classList.add("tab");
	return results;
}

function replaceResults(results){
	var body = document.getElementsByTagName("body")[0];
	var oldResult = document.getElementById("results");
	body.replaceChild(results, oldResult);
}

function handleResponse(obj, callback) {
	var results = document.createElement("div")
	results.id = "results";
	results.classList.add("tab");
	results.classList.add("results");
	if(obj.success != true){
		var div = document.createElement("div")
		div.classList.add('result');
		div.appendChild(createTextElement('h2', 'Failed to get data from API :('));
		div.appendChild(createTextElement('h3', 'Reason: ' + obj.error));
		results.appendChild(div);
	}
	else
		callback(results, obj)

	var body = document.getElementsByTagName("body")[0];
	var oldResult = document.getElementById("results");
	body.replaceChild(results, oldResult);
}

function createRow(name, value){
	var tr = document.createElement('tr');
	tr.appendChild(createTextElement('th', name));
	if(value instanceof HTMLElement){
		var td = document.createElement('td');
		td.appendChild(value);
		tr.appendChild(td);
	}
	else {
		tr.appendChild(createTextElement('td', value))
	}
	return tr;
}


function createTextElement(type, text){
	var obj = document.createElement(type);
	obj.innerHTML = text;
	return obj;
}

function capitalize(string){
	if(string.lenght < 1)
		return string;
	var s = string.charAt(0).toUpperCase()
	if(string.length > 1)
		s += string.slice(1);
	return s;
}

/*
 * Reload the page with any new get variables
 */
function reloadPage(variables){
	var url = window.location.href.split('?');
	window.location.href = url[0] + '?' + variables;
}

function deleteCookies(){
	var cookies = document.cookie.split(";");

	for (var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i];
		var eqPos = cookie.indexOf("=");
		var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
		document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
	}
	window.location.href = window.location.href.split('?')[0];
}

