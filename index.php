<?php

$translations = json_decode(file_get_contents("translations.json"), true);

$lang = "en";

// See if get variable 'lang is set
if(isset($_GET['lang'])){
	$lang = $_GET['lang'];
	setcookie('lang', $lang, time() + (84000*7), '/');
}

// Otherwise fetch language from cookie if it exist
else if(isset($_COOKIE['lang'])){
	$lang = $_COOKIE['lang'];
}

// Restrict languages to only support these
$languages = array("en", "br", "cy", "ga", "gd");
if(!in_array($lang, $languages))
	$lang = "en";


/*
 * Fetches the text with the given string in translations.
 * If the translations contains a translation for the current language return that,
 * otherwise return the English version.
 */
function s($key){
	global $translations, $lang;
	if(!isset($translations[$key]))
		throw new Exception('Missing key: ' + $key);
	if(isset($translations[$key][$lang]))
		return $translations[$key][$lang];
	return $translations[$key]["en"];
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Open Celtic Dictionary; Digi Web-Interface</title>
		<meta charset="utf-8"/>
		<meta name="author" content="The Open Celtic Dictionary Project">
		<meta name=”description” content="The Open Celtic Dictionary Project. Free and open dictionary for Welsh, Breton, Irish, and Scottish Gaelic.">
		<meta name="keywords" content="Dictionary, Welsh, Breton, Irish, Gaelic, Scottish Gaelic, Open">
		<script type="text/javascript" src="script.js"></script>
		<script type="text/javascript" src="scripts/number.js"></script>
		<script type="text/javascript" src="scripts/trans.js"></script>
		<script type="text/javascript" src="scripts/word.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="icon" type="image/png" href="digi_v2.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<h1><?=s('header')?></h1>
		<nav>
			<button class="tabButton" onclick="openTab(['searchSettings', 'results'])"><?=s('dictionary')?></button>
			<button class="tabButton" onclick="openTab(['numberSettings', 'results'])"><?=s('numbers')?></button>
			<button class="tabButton" onclick="openTab(['transSettings', 'results'])"><?=s('translations')?></button>
			<button class="tabButton" onclick="openTab(['about'])"><?=s('about')?></button>
			<button class="tabButton" onclick="openTab(['settings'])"><?=s('settings')?></button>
		</nav>
		<div id="searchSettings" class="tab">
			<div>
				<select id="lang" class="select-css">
					<option value="br"><?=s('breton')?></option>
					<option value="ga"><?=s('irish')?></option>
					<option value="gd"><?=s('scottish_gaelic')?></option>
					<option value="cy" selected><?=s('welsh')?></option>
				</select>
				<select id="type" class="select-css" >
					<option value="verb"><?=s('verb')?></option>
					<option value="adjective"><?=s('adjective')?></option>
					<option value="noun"><?=s('noun')?></option>
					<option value="preposition"><?=s('preposition')?></option>
					<option value=""><?=s('all')?></option>
				</select>
				<select id="selectSearch" class="select-css">
					<option value="false"><?=s('dictionary_entry_search')?></option>
					<option value="true"><?=s('extensive_search')?></option>
				</select>
			</div>
			<div>
				<input type="text" id="search" placeholder="<?=s('placeholder_type_word')?>" onkeypress="return keyPress(event)"/>
				<input type="button" class="go" value="<?=s('submit')?>" onclick="wordSearch()">
			</div>
		</div>
		<div id="numberSettings" class="tab" >
			<input type="number" id="numberSearch" placeholder="<?=s('placeholder_type_number')?>" onkeypress="return keyPress(event)"/>
			<input type="button" class="go" value="<?=s('submit')?>" onclick="numberSearch()">
		</div>
		<div id="transSettings" class="tab" >
			<input type="text" id="transSearch" placeholder="<?=s('placeholder_type_english_word')?>" onkeypress="return keyPress(event)"/>
			<input type="button" value="<?=s('submit')?>" onclick="transSearch()">
		</div>
		<div id="about" class="tab results">
			<div class="result">
				<h2>The Open Celtic Dictionary Project</h2>
				<h3>About the project</h3>
				<p>
				The project was stared during the spring of 2020 in order to try to make an open-source dictonary API for all of the 6 currently living Celtic Languages.
				We are always looking for new volunteers, so if you want to help add new words, report any bugs, improve the website, or have other ideas please contact us on Discord or Gitlab (links bellow) :)
				</p>
				<p>
				The project also have it's own Discord bot which you are able to invite to your server.
				His name is Digi and can be found <a href="https://gitlab.com/prvInSpace/digi">here.</a>
				He can be invited to any Discord server using the following link: 
				<a href="https://discord.com/api/oauth2/authorize?client_id=695552413066854502&permissions=346176&scope=bot">Click me!</a>
				</p>
				<h3>Special Thanks</h3>
				<p>
				I want to thank everyone who as contributed in anyway to this project. Without your support this would never have been possible. So from the bottom of my heart: Thank you!
				<br /><br/>
				- Preben 'prv' Vangberg.
				<p>
				<h3>Resources</h3>				
				<table>
					<tr>
						<th>Gitlab</th>
						<td><a href="https://gitlab.com/prvInSpace/open-celtic-dictionary">Link to repo</a></td>
					</tr>
					<tr>
						<th>Discord</th>
						<td><a href="https://discord.gg/UzaFmfV">Invite Link</a></td>
					</tr>
					<tr>
						<th>Digi, the Discord bot</th>
						<td><a href="https://discord.com/api/oauth2/authorize?client_id=695552413066854502&permissions=346176&scope=bot">Invite link</a></td>
					</tr>
					<tr>
						<th>Twitter</th>
						<td><a href="https://twitter.com/OpenCelticDict">Link to Twitter</a></td>
					</tr>
				</table>
				<h3>Usage notes:</h3>
				<h4>Word types</h4> 
				<p>
				Word types are used for generating the results. The word type 'all' can be used, but will only work for confirmed word (i.e words that are in the dictionary).
				Searches with the word type 'all' will never generate the result.
				</p>
				<h4>Normal vs Extensive search</h4>
				<p>
					<strong>Normal search</strong> is used to lookup words by their dictionary entry (i.e the lemma of the word). If a word is not present in the dictionary it will be
					generated if the word type is not set to 'all'.
				</p>
				<p>
					<strong>Extensive search</strong> is used to lookup words by conjugated terms. This implies the word type 'all' and will never generate a result. 
				</p>
			</div>
		</div>
		<div id="settings" class="tab results">
			<div class="result">
				<h2>Settings</h2>
				<p><strong>
					NB: By changing any of these settings you are concenting to the use of cookies in order for the website to remember these settings
					For more information read our <button class="clickableWord" onclick="openTab(['cookies'])" style="font-weight: bold;">cookie policy.</button>
				</strong><p>
				<h3>Language</h3>
				<p>
					You are able to change the interface language here. Translations might not be complete.
				</p>
				<table>
					<tr>
						<td>Language</td>
						<td>
							<select onchange=reloadPage('lang='+this.options[this.selectedIndex].value)>
								<option value="en" <?php if($lang=="en")echo "selected";?>>English</option>
								<option value="br" <?php if($lang=="br")echo "selected";?>>Breton | Brezhoneg</option>
								<option value="ga" <?php if($lang=="ga")echo "selected";?>>Irish | Gaeilge</option>
								<option value="gd" <?php if($lang=="gd")echo "selected";?>>Scottish Gaelic | Gàidhlig</option>
								<option value="cy" <?php if($lang=="cy")echo "selected";?>>Welsh | Cymraeg</option>
							</select>
						</td>
					</tr>
				</table>
				<h3>Cookies</h3>
				<input type="button" value="Delete all cookies and remove all settings" onclick="deleteCookies()">
			</div>
		</div>
		<div id="cookies" class="tab results">
			<div class="result">
			<h2>Cookie Policy</h2>
			<p>
				This webpage only uses cookies to store user settings.
				It will only do this if the user have decided to change the settings, or if the user entered an url which set the cookie.
				This page will never use any form of advertising cookie or other form of cookie that contradict the purpose expressed above. 
			</p>
			<h3>Deleting Cookies</h3>
			<p>
				Cookies can easily be deleted using a button on the settings page.
			</p>
			<h3>Cookies</h3>
			<table>
				<tr>
					<th>Name</th>
					<th>Purpose</th>
				</tr>
				<tr>
					<td>lang</td>
					<td>This cookie is used to store the desired language of the website.</td>
				</tr>
			</table>
			</div>
		</div>
		<div id="results" class="tab results"></div>		
<script>
const lang = "<?=$lang?>";
<?php
if(isset($_GET["id"]))
	echo "const startUpId = ", $_GET["id"], ';';
?>
</script>
	</body>
</html>


<?php
$file_path = "visitors.data";
$handle = fopen($file_path, "r");
if(!$handle)
	$counter = 0;
else {
	$counter = (int) fread($handle, 20);
	fclose($handle);
}
$counter++;
$handle = fopen($file_path, "w");
if($handle){
	fwrite($handle, $counter);
	fclose($handle);
}
?>



