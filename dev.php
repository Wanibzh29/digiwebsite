<!doctype html>
<html>
	<head>
		<title>Development Tools; Open Celtic Dictionary</title>
		<meta charset="utf-8"/>
		<script type="text/javascript" src="script.js"></script>
		<script type="text/javascript" src="scripts/number.js"></script>
		<script type="text/javascript" src="scripts/word.js"></script>
		<script type="text/javascript" src="scripts/idlist.js"></script>
		<script type="text/javascript" src="scripts/generatejson.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="icon" type="image/png" href="digi_v2.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<h1>Development Tools for the Open Celtic Dictionary</h1>
		<nav>
			<button class="tabButton" onclick="openTab(['testJson', 'results'])">JSON Tester</button>
			<button class="tabButton" onclick="openTab(['generateJson', 'results'])">JSON Generator</button>
			<button class="tabButton" onclick="openTab(['idlist', 'results'])">ID List</button>
			<a href="/">Mainpage</a>
		</nav>
		<div id="testJson" class="tab defaultOpen">
			<div>
				<select id="lang">
					<option value="cy">Welsh</option>
					<option value="ga">Irish</option>
					<option value="br">Breton</option>
					<option value="gd">Scottish Gaelic</option>
				</select>
				<select id="type">
					<option value="verb">Verb</option>
					<option value="adjective">Adjective</option>
					<option value="noun">Noun</option>
					<option value="preposition">Preposition</option>
				</select>
			</div>
			<div>
				<input type="text" id="json" placeholder="Paste JSON here..." onkeypress="return keyPress(event)"/>
				<input type="button" value="Submit" onclick="generateWord()">
			</div>
		</div>
		<div id="generateJson" class="tab">
			<label for="startIndex">Start index</label>
			<input type="number" id="startIndex">
			<input type="text" id="newWord" placeholder="Add word here" onkeypress="return keyPress(event)">
			<input type="button" value="Generate" onclick="generateJson()">
			<input type="button" value="Clear words" onclick="clearWordList()">
		</div>
		<div id="idlist" class="tab">
			<label for="min">Minimum:</label>
			<input type="number" id="min" name="min" min="0" max="2000000">
			<label for="max">Maximum:</label>
			<input type="number" id="max" name="max" min="1" max="2000000">
			<input type="button" value="Submit" onclick="generateList()">
		</div>
		<div id="results" class="tab"></div>		
	</body>
</html>
