
function wordListToDiv(results, obj){
	if(obj.results.length > 0){
		obj.results.forEach(function(word){
			results.appendChild(convertWordToDiv(word))
		});
	}
	else {
		var div = document.createElement("div")
		div.classList.add('result');
		div.appendChild(createTextElement('h2', 'No results found'));
		div.appendChild(createTextElement('h3', ':('));
		results.appendChild(div);
	}
}

function convertWordToDiv(word) {
	var div = document.createElement("div")
	div.classList.add('result');
	var normalForm = createTextElement("h2", capitalize(word.normalForm));
	var mutations = createTextElement("h3", s("mutations"));
	var conjugations = createTextElement("h3", s("conjugations"));
	var inflections = createTextElement("h3", s("inflections"))

	div.appendChild(normalForm);
	if('id' in word){
		var button = document.createElement('button');
		button.onclick = function(){
			var linkBox = document.createElement('input');
			var link = "http://digi.prv.cymru?id=" + word.id;
			linkBox.value = link;
			linkBox.readOnly = true;
			linkBox.type = "text";
			div.replaceChild(linkBox, button);
			linkBox.focus();
			linkBox.select();
		}
		button.innerHTML = "Generate link";
		div.appendChild(button);
	}
	div.append(createTextElement('h3', s('basic_information')));
	div.appendChild(createBaseInformationTable(word));
	var translations = createTranslationTable(word.translations, [word.lang]);
	if(translations){
		div.append(createTextElement('h3', s('translations')));
		div.append(translations);
	}

	if ("mutations" in word)
		if (word.mutations.soft !== "" || word.mutations.nasal !== "" || word.mutations.aspirate !== ""){
			div.appendChild(mutations);
			div.appendChild(getMutationTable(word));

		}
	if (word.type == "adjective")
		div.appendChild(printAdjective(word));

	if(word.type == "preposition" && word.inflected){
		div.appendChild(inflections);
		div.appendChild(createWordTenseTable(s("inflections"), word));
	}

	if(word.type == "verb")
		div.appendChild(conjugations);

	tenses.forEach(function(key){
		if(key in word)
			div.append(createWordTenseTable(s('tense_' + key), word[key]));
	});

	return div;
}


function createBaseInformationTable(word){
	var table = document.createElement("table");
	table.appendChild(createRow(s('language'), s(languageCodeToName[word.lang])));
	table.appendChild(createRow(s('type'), s(word.type)));
	table.appendChild(createRow(s('status'), word.confirmed ? s('confirmed') : s("generated")));
	if('plural' in word)
		table.appendChild(createRow(s('plural'), word.plural.join('<br />')));
	if('etymology' in word)
		table.appendChild(createRow(s('etymology'), word.etymology));
	if('versions' in word && word.versions.length > 0)
		table.appendChild(createRow(s('versions'), createListOfIdValueWords(word.versions)));
	if('notes' in word)
		table.appendChild(createRow(s('notes'), createTextElement('p', word.notes)));
	if('related' in word && word.related.length > 0)
		table.appendChild(createRow(s('related'), createListOfIdValueWords(word.related)));
	if('synonyms' in word && word.synonyms.length > 0)
		table.appendChild(createRow(s('synonyms'), createListOfIdValueWords(word.synonyms)));
	if('antonyms' in word && word.antonyms.length > 0)
		table.appendChild(createRow(s('antonyms'), createListOfIdValueWords(word.antonyms)));
	return table;
}

function createListOfIdValueWords(words){
	var p = document.createElement('div');
	words.forEach(function(word){
		if('id' in word)
			p.appendChild(createClickableWord(word.value, word.id));
		else
			p.appendChild(createTextElement('span', word.value));
		p.appendChild(document.createElement('br'));
	});
	return p;
}

function createTranslationTable(translationUnits, exlude = [] ){
	var translations = {};
	var found = {};
	translationUnits.forEach(function(unit){
		for(var language in unit){
			if(exlude.includes(language))
				continue;
			if(!(language in translations)){
				translations[language] = [];
				found[language] = [];
			}
			unit[language].forEach(function(translation){
				if(found[language].indexOf(translation.value) < 0){
					found[language].push(translation.value);
					if('id' in translation){
						translations[language].push(createClickableWord(translation.value, translation.id));
					}
					else
						translations[language].push(createTextElement('span', translation.value));
				}
			});
		}
	});
	if(Object.keys(translations).length == 0)
		return null;
	var table = document.createElement('table');
	for(var lang in translations){
		var tr = document.createElement('tr');
		tr.appendChild(createTextElement('th', s(languageCodeToName[lang])));
		var td = document.createElement('td');
		for(var i = translations[lang].length -1; i >= 0; --i){
			td.appendChild(translations[lang][i]);
			if(i != 0)
				td.appendChild(document.createElement('br'))
		}
		tr.appendChild(td);
		table.appendChild(tr);
	}
	return table;
}

function getMutationTable(word){
	var table = document.createElement("table");
	var head = document.createElement("tr");
	var row = document.createElement("tr");

	var mutationTable = mutationsTable[word.lang];
	for(var mutation in mutationTable){
		head.appendChild(createTextElement('th', s('mutation_' + mutation)));
		row.appendChild(createTextElement('td', word.mutations[mutation]));
	}

	table.appendChild(head);
	table.appendChild(row);
	return table;
}

function createWordTenseTable(header, tense){
	var table = document.createElement("table");
	table.classList.add('tense');
	var head = document.createElement("tr");
	head.appendChild(createTextElement("th", header));
	head.appendChild(createTextElement("th", s("singular")));
	head.appendChild(createTextElement("th", s("plural")));
	table.appendChild(head)	
	// Eg og vi

	if (("singFirst" in tense &&tense.singFirst.length > 0) || ("plurThird" in tense && tense.plurFirst.lenght > 0)){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("first")));
		row1.appendChild(createTextElement("td", "singFirst" in tense ? tense.singFirst.join("<br />") : ""));
		row1.appendChild(createTextElement("td", "plurFirst" in tense ? tense.plurFirst.join("<br />") : ""));
		table.appendChild(row1);
	}
	// Du og dere 
	if (("singSecond" in tense && tense.singSecond.length > 0) || ("plurSecond" in tense && tense.plurSecond.length > 0)){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("second")));
		row1.appendChild(createTextElement("td", "singSecond" in tense ? tense.singSecond.join("<br />") : ""));
		row1.appendChild(createTextElement("td", "plurSecond" in tense ? tense.plurSecond.join("<br />") : ""));
		table.appendChild(row1);
	}
	// Han ho, de, dei
	if (("singThird" in tense && tense.singThird.length > 0) || ("plurThird" in tense && tense.plurThird.length > 0)){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("third")));
		row1.appendChild(createTextElement("td", "singThird" in tense ? tense.singThird.join("<br />") : ""));
		row1.appendChild(createTextElement("td", "plurThird" in tense ? tense.plurThird.join("<br />") : ""));
		table.appendChild(row1);
	}
	// Han
	if ("singThirdMasc" in tense && tense.singThirdMasc.length > 0){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("third_masculine")));
		row1.appendChild(createTextElement("td", tense.singThirdMasc.join("<br />")));
		table.appendChild(row1);
	}
	//ho
	if ("singThirdFem" in tense && tense.singThirdFem.length > 0){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("third_femenine")));
		row1.appendChild(createTextElement("td", tense.singThirdFem.join("<br />")));
		table.appendChild(row1);
	}
	// Nåke
	if ("impersonal" in tense && tense.impersonal.length > 0){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("impersonal")));
		row1.appendChild(createTextElement("td", tense.impersonal.join("<br />")));
		table.appendChild(row1);
	}
	// Voodoo shit
	if ("analytic" in tense && tense.analytic.length > 0){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("analytic")));
		row1.appendChild(createTextElement("td", tense.analytic.join("<br />")));
		table.appendChild(row1);
	}

	return table;
}

function printAdjective(word){
	var table = document.createElement("table");
	var head = document.createElement("tr");
	head.appendChild(createTextElement("th", s("equative")));
	head.appendChild(createTextElement("th", s("superlative")));
	head.appendChild(createTextElement("th", s("comparative")));
	if('exclamative' in word)
		head.appendChild(createTextElement('th', s("exclamative")));
	table.appendChild(head);
	var row1 = document.createElement("tr");
	row1.appendChild(createTextElement("td", word.equative.join("<br />")));
	row1.appendChild(createTextElement("td", word.superlative.join("<br />")));
	row1.appendChild(createTextElement("td", word.comparative.join("<br />")));
	if('exclamative' in word)
		row1.appendChild(createTextElement('td', word.exclamative.join("<br />")));
	table.appendChild(row1);
	return table;
}

const mutationsTable = {
	"cy": {
		"init": "Unmutated",
		"soft": "Soft",
		"nasal": "Nasal",
		"aspirate": "Aspirate"
	},
	"br": {
		"init": "Unmutated",
		"soft": "Soft",
		"spirant": "Spirant",
		"hard": "Hard",
		"mixed": "Mixed"
	}
}

const languageCodeToName = {
	"en": "english",
	"cy": "welsh",
	"br": "breton",
	"ga": "irish",
	"gd": "scottish_gaelic",
	"no": "norwegian"
}

const tenses = [
	"present", 
	"present_independent", 
	"present_dependent", 
	"present_affirmative",
	"present_negative", 
	"present_interrogative",
	"present_situative",
	"present_habitual",
	"past", 
	"preterite", 
	"past_independent", 
	"past_dependent", 
	"past_habitual", 
	"imperfect", 
	"imperfect_situative",
	"imperfect_habitual",
	"imperfect_affirmative",
	"imperfect_negative",
	"imperfect_interrogative",
	"future", 
	"future_independent", 
	"future_dependent", 
	"future_relative", 
	"conditional", 
	"conditional_present", 
	"conditional_imperfect", 
	"conditional_independent", 
	"conditional_dependent", 
	"imperative"
]

